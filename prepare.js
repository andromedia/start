const fs = require("fs");
const http = require("isomorphic-git/http/node");
const { spawn } = require("child_process");
const git = require("isomorphic-git");
const { join } = require("path");
const mode = process.argv[2];
const rimraf = require("rimraf-promise");

const prep = async () => {
    let shouldYarn = true;

    // bootstrap the cli env
    const cliDir = join(process.cwd(), "libs", "cli");
    const coreDir = join(process.cwd(), "libs", "core");

    if (!fs.existsSync(cliDir)) {
        fs.mkdirSync(cliDir, { recursive: true });
    }

    if (mode === "update-libs") {
        console.log(`Updating libs...`);

        if (fs.existsSync(join(cliDir, "src", "index.ts"))) {
            console.log("You are in libs:dev mode, please pull changes manually...");
        } else {
            await rimraf(cliDir);
            await rimraf(coreDir);
            await git.clone({
                fs,
                http,
                dir: join(cliDir, "dist"),
                url: 'https://bitbucket.org/andromedia/core-cli',
                ref: "master",
                singleBranch: true,
                depth:1
            });
            await rimraf(join(cliDir, "dist", ".git"));
            fs.renameSync(join(cliDir, "dist", "package.json"), join(cliDir, "package.json"));

            await git.clone({
                fs,
                http,
                dir: join(coreDir, "dist"),
                url: 'https://bitbucket.org/andromedia/core-libs',
                ref: "master",
                singleBranch: true,
                depth:1
            });
            await rimraf(join(coreDir, "dist", ".git"));
            fs.renameSync(join(coreDir, "dist", "package.json"), join(coreDir, "package.json"));
        }
        await run("yarn");
        return;
    }


    if (!fs.existsSync(join(cliDir, "package.json")) || mode === "dev") {
        console.log(`Preparing Project ${mode === "dev" ? "[Core Dev] ..." : "..."}`);
        let shouldClone = true;
        let depth = 1;
        removeCLIFromWorkspace(cliDir);
        if (mode === "dev") {
            depth = undefined;
            if (!fs.existsSync(join(cliDir, "package.json"))) {
                await rimraf(cliDir);
            } else {
                shouldClone = false;
                shouldYarn = false
            }
        }

        if (shouldClone) {
            if (mode !== "dev") {
                await git.clone({
                    fs,
                    http,
                    dir: join(cliDir, "dist"),
                    url: 'https://bitbucket.org/andromedia/core-cli',
                    ref: "master",
                    singleBranch: true,
                    depth
                });
                await rimraf(join(cliDir, "dist", ".git"));
                fs.renameSync(join(cliDir, "dist", "package.json"), join(cliDir, "package.json"));

                await git.clone({
                    fs,
                    http,
                    dir: join(coreDir, "dist"),
                    url: 'https://bitbucket.org/andromedia/core-libs',
                    ref: "master",
                    singleBranch: true,
                    depth
                });
                await rimraf(join(coreDir, "dist", ".git"));
                fs.renameSync(join(coreDir, "dist", "package.json"), join(coreDir, "package.json"));
            } else {
                await git.clone({
                    fs,
                    http,
                    dir: cliDir,
                    url: 'https://bitbucket.org/andromedia/core-cli',
                    ref: "dev",
                    singleBranch: true,
                    depth
                });
                await rimraf(join(cliDir, "dist"));
                await git.clone({
                    fs,
                    http,
                    dir: join(cliDir, "dist"),
                    url: 'https://bitbucket.org/andromedia/core-cli',
                    ref: "master",
                    singleBranch: true
                });

                await rimraf(join(coreDir));
                await git.clone({
                    fs,
                    http,
                    dir: join(coreDir),
                    url: 'https://bitbucket.org/andromedia/core-libs',
                    ref: "dev",
                    singleBranch: true
                });

                await rimraf(join(coreDir, "dist"));
                await git.clone({
                    fs,
                    http,
                    dir: join(coreDir, "dist"),
                    url: 'https://bitbucket.org/andromedia/core-libs',
                    ref: "master",
                    singleBranch: true
                });

                await git.clone({
                    fs,
                    http,
                    dir: join(process.cwd(), "app"),
                    url: "https://bitbucket.org/andromedia/core-base",
                    ref: "master",
                    singleBranch: true,
                    depth: 1,
                });
                addCoreToWorkspace();
            }
        }

        if (fs.existsSync(join(cliDir, "package.json"))) {
            addCLIToWorkspace(cliDir);
        }

        if (shouldYarn) {
            await run("yarn");
            if (mode === "dev") {
                await run("yarn cli dev:all");
            }
        }
    }

};

const removeCLIFromWorkspace = (cliDir) => {
    const pkg = JSON.parse(fs.readFileSync("package.json", "utf-8"));

    if (!pkg.workspaces) {
        pkg.workspaces = [];
    }

    if (pkg.workspaces.indexOf("libs/cli") >= 0) {
        pkg.workspaces.splice(pkg.workspaces.indexOf("libs/cli"), 1);
    }

    if (!pkg.dependencies) {
        pkg.dependencies = {};
    }

    if (pkg.dependencies) {
        delete pkg.dependencies.cli
    }

    fs.writeFileSync("package.json", JSON.stringify(pkg, null, 2));
}

const addCLIToWorkspace = (cliDir) => {
    const pkg = JSON.parse(fs.readFileSync("package.json", "utf-8"));

    if (!pkg.workspaces) {
        pkg.workspaces = [];
    }

    if (pkg.workspaces.indexOf("libs/cli") < 0) {
        pkg.workspaces.push("libs/cli");
    }

    if (!pkg.dependencies) {
        pkg.dependencies = {};
    }

    if (pkg.dependencies) {
        pkg.dependencies = {
            ...pkg.dependencies,
            "cli": "workspace:libs/cli",
        };
    }

    fs.writeFileSync("package.json", JSON.stringify(pkg, null, 2));
}

const run = (command, silent = false) => {
    return new Promise((resolve) => {
        const cwd = join(process.cwd());
        const str = command.split(" ");
        const cmd = str.shift();
        const child = spawn(cmd, [...str], { stdio: silent ? 'ignore' : 'inherit', cwd });
        child.on('close', () => resolve());
        child.on('exit', () => resolve());
    });
}

const addCoreToWorkspace = () => {
    const pkg = JSON.parse(fs.readFileSync("package.json", "utf-8"));

    if (!pkg.workspaces) {
        pkg.workspaces = [];
    }

    if (pkg.workspaces.indexOf("app") < 0) {
        pkg.workspaces.push("app");
        pkg.workspaces.push("libs/core");
    }

    if (!pkg.dependencies) {
        pkg.dependencies = {};
    }

    if (pkg.dependencies) {
        pkg.dependencies = {
            ...pkg.dependencies,
            app: "workspace:app",
            core: "workspace:libs/core"
        };
    }

    fs.writeFileSync("package.json", JSON.stringify(pkg, null, 2));
};


prep();